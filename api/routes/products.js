const express = require('express');
const router = express.Router();
const multer = require('multer');

const Product = require('../models/product');
const checkAuth = require('../middleware/check-auth');
const productsController = require('../controllers/products');


const storage = multer.diskStorage({
    destination: function(req, file, cb){
        cb(null, './uploads/');
    },

    filename: function(req, file, cb){
        cb(null, new Date().getFullYear().toString()+ '-' + new Date().getMonth().toString() +'-'+ new Date().getDay().toString() +'-'+ new Date().getMilliseconds().toString() +'-'+ file.originalname);
    }
});

const fileFilter = (req, file, cb) => {
    //reject ta file
    if(file.mimetype ==='image/jpeg' || file.mimetype ==='image/png' || file.mimetype ==='image/jpg' || file.mimetype ==='image/jpg')
        cb(null, true);
    else
        cb(null, false);
};
const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter
});

router.get('/', productsController.get_product_all);

router.post('/', checkAuth, upload.single('productImage'), productsController.product_create );


router.get('/:productId', productsController.get_product_single);

router.patch('/:productId', checkAuth, productsController.product_update);


router.delete('/:productId', checkAuth, productsController.product_delete);
module.exports = router;