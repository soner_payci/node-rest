const Product = require('../models/product');

const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
exports.get_product_all = (req, res, next ) => {
    Product.find()
    .select('name price _id productImage')
    .exec()
    .then(docs => {
        const response = {
            count: docs.length,
            products: docs.map(doc => {
                return {
                    name: doc.name,
                    price: doc.price,
                    productImage: doc.productImage,
                    _id: doc._id,
                    request: {
                        type: 'GET',
                        url: 'http://localhost:3000/products/'+ doc._id
                    }
                }
            })
        }
    //    if(docs.length >= 0)   Bu kısım isteğe kalmış ama sonuçta hiç ürün yoksa null değil de boş bir array dönüyor. Buna gerçek bir hataymış gibi yaklaşamayız.

            res.render('products.ejs', {response : response});

           // res.status(200).json(response);
    //    else
    //        res.status(404).json({message: 'No entries found for products.'});  
    })
    .catch(err=> {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
}

exports.product_create = (req, res, next ) => {
    console.log(req.file);
    
    const product = new Product({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        price: req.body.price,
        productImage: req.file.path
    });
    product.save().then(result => {
        console.log(result);
        res.status(201).json({
            message: "Created product successfully",
            createdProduct: {
                name: result.name,
                price: result.price,
                _id: result._id,
                request: {
                    type: 'GET',
                    url: 'http://localhost:3000/products'+ result._id
                }
            }
        });
    }).catch(err =>{
        console.log(err);
        res.status(500).json({
            error: err
        })
    });

}

exports.get_product_single = (req ,res , next) => {
    const id = req.params.productId;
    Product.findById(id)
    .select('name price _id productImage')
    .exec()
    .then(
        doc => {
            console.log("From Database: ", doc);
            if(doc)
                res.status(200).json({
                    product: doc,
                    request: {
                        type: 'GET',
                        //description: 'Get_All_Products_Link', istediğimiz herhangi bir şey yazabiliriz böyle.
                        url: 'http://localhost:3000/products'
                    }
                });
            else
                res.status(404).json({message: 'No valid entry found for provided ID => '+ id});
        }).catch(
            err => {
                console.log(err);
                res.status(500).json({error: err});
            });
}

exports.product_update = (req ,res , next) => {
    const id = req.params.productId;
    const updateOps = {};
    for (const ops of req.body)
        updateOps[ops.propName] = ops.value;
    Product.update({_id: id}, { $set: updateOps })
    .exec()
    .then(result => {
        console.log(result);
        res.status(200).json({
            message: 'Product updated',
            request: {
                type: 'GET',
                //description: 'Get_All_Products_Link', istediğimiz herhangi bir şey yazabiliriz böyle.
                url: 'http://localhost:3000/products' + id
            }
        });
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
}

exports.product_delete = (req ,res , next) => {
    const id = req.params.productId;
    Product.remove({_id: id})
    .exec()
    .then(result => {
        res.status(200).json(result);
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
 }