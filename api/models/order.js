const mongoose = require('mongoose');

const productSchema = mongoose.Schema(
    {
        _id: mongoose.Schema.Types.ObjectId,
        product: {type: mongoose.Schema.Types.ObjectId, ref: 'Product', required: true}, //ref ile bunun baglı olacağı obje türünü seçiyoruz. 
        quantity: {type: Number, default: 1}
    }
);

module.exports = mongoose.model('Order', productSchema);